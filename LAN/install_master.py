import sys, glob, os,time
from subprocess import call
def make_file(file):
    print(f'creating {file}...')
    code=[]
    with open(sys.argv[0], 'r') as f:
        lines = f.readlines()
    main_area = False
    for line in lines:
        if line == (f'### end of {file} ###') or line == f'### end of {file} ###\n':
            main_area = False
            break
        if main_area:
            code.append(line.replace('###',''))
        if line == f'### start of {file} ###\n':
            main_area = True
    with open(file, 'w') as f:
        for line in code:
            f.write(line)
    print('done')
    return(code)
make_file("fan.py")
make_file("fan.sh")
make_file("config.cf")
print('delete install files')
os.chdir("./")
if os.path.isfile("./install_master.sh"):
    os.unlink("install_master.sh")
os.unlink("install.py")
print("done")
print('edit config.cf settings')
#input('press enter to continue')
exit()
'''
### start of fan.sh ###
### sudo python3 ./fan.py
### end of fan.sh ###
### start of config.cf ###
debug True
raspberry True
pwm_frequency 100
interval 1
minimal_fan_speed 20
fan_off 30
fan_full_on 50
buffer 5
fan_pin 11
timeout 10
addr []
### end of config.cf ###
### start of fan.py ###
def getbool(name):
    config_file=open('config.cf','r')
    line = config_file.readline()
    while not(name in line):
        line = config_file.readline()
    config_file.close()
    return("True" in line or "true" in line)
def getlist(name):
    val=[]
    config_file=open('config.cf','r')
    line = config_file.readline()
    while not(name in line):
        line = config_file.readline()
    if (not(("[" in line) and ("]" in line))):
        config_file.close()
        return([])
    else:
        config_file.close()
        string = str(line.replace(name,"").replace(" ","").replace("[","").replace("]","").replace("\n",""))
        if (string == ""):
            config_file.close()
            return([])
        this = ""
        for byte in string:
            if byte==",":
                try:
                    val.append(int(this))
                except:
                    val.append(this)
                this = ""
            else:
                this += byte
        try:
            val.append(int(this))
        except:
            val.append(this)
    return (val)
def getint(name):
    val = ""
    config_file=open('config.cf','r')
    line = config_file.readline()
    while not(name in line):
        line = config_file.readline()
    config_file.close()
    string = str(line.replace(name,"").replace(" ",""))
    this = ""
    for byte in string:
         try:
             int(byte)
             val += byte
         except:
            while False:
                print("this does not execute")
    return int(val)
def get_ip_address():
    ip_address = '';
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8",80))
    ip_address = s.getsockname()[0]
    s.close()
    return ip_address
rasp = getbool("raspberry")
debug = getbool("debug")
addr = getlist("addr")
off = getint("fan_off")
full_on = getint("fan_full_on")
pwm_frequency = getint("pwm_frequency")
interval = getint("interval")
minimal_fan_speed = getint("minimal_fan_speed")
fan_pin = getint("fan_pin")
import time
import json
import asyncio
if rasp:
    import RPi.GPIO as GPIO
    import socket
    import signal
    import sys
    import os
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(fan_pin,GPIO.OUT)
    fan = GPIO.PWM(fan_pin,pwm_frequency)
    fan.start(100)
    time.sleep(1)
    fan_pin_p = fan_pin
    res = os.popen('hostname -I').readline().split(" ")[0]
    print(f'ip: {res}')
    if debug:
        print('start')
    else:
        while False:
            print("this does not execute")
else:
    debug = True
if debug:
    print ("LAN addr: " + str(addr) + " fan_off: " + str(off) + " fan_full_on " + str(full_on))
speed = 0
def mapval(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
def gettemp():
    try:
        if rasp:
            temp = []
            #res = os.popen('vcgencmd measure_temp').readline()
            #_temp = res.replace("temp=","").replace("'C\n","").replace(".0","")
            t=open("/sys/class/thermal/thermal_zone0/temp")
            _temp = t.readline()
            t.close()
            _temp = float(_temp)/1000
            if debug:
                print("temp:\t\t {0}".format(_temp))
            temp.append(_temp)
            if debug:
                print("get LAN addr`s:\t " + str(addr))
            if (addr.__len__() < 1):
                return float(_temp)
            for _addr in addr:
                if debug:
                    print("get LAN addr:\t " + str(_addr))
                ip = _addr.split(":")[0]
                port = _addr.split(":")[1]
                try:
                    ask = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
                    ip = _addr.split(":")[0]
                    port = int(_addr.split(":")[1])
                    if (ip == get_ip_address()):
                        ask.sendto(bytes(str(200), 'utf-8'), (ip, port-1))
                    else:
                        ask.sendto(bytes(str(200), 'utf-8'), (ip, port))
                    ask.close()
                    receive = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
                    receive.settimeout(getint("timeout"))
                    receive.bind((get_ip_address(),int(port)))
                    _temp = receive.recvfrom(1024)
                    receive.close()
                    _temp = float(str(_temp[0]).replace("b'","").replace("'","")) 
                    if debug: 
                        print(f'{ip}\'s temp: {_temp}')
                    temp.append(_temp)
                except:
                    if debug:
                        print("fail to get temperature of \"" + str(_addr)+ "\"")

            if debug:
                print("temp:\t\t {0}".format(temp))
            temperature = max(temp)
            return float(temperature)
    except:
        if debug:
            print("fail to get temp")
        try:
            t.close()
        except:
            try:
                t=open("/sys/class/thermal/thermal_zone0/temp")
                _temp = t.readline()
                t.close()
                _temp = float(_temp)/1000
            except:
                while False:
                    print("this does not execute")
        return full_on
    if not(rasp):
        temp = []
        temp.append(int(input("enter my temp: ")))
        for _addr in addr:
            print(_addr, "temp")
            _temp = int(input("enter " + str(_addr) + " temp: "))
            temp.append(_temp)
        print(temp)
        temperature = max(temp)
        print(temperature)    
        return int(temperature)
fan_off = False
try:    
    while True:
        if debug:
            print("\n")
        debug = getbool("debug")
        addr = getlist("addr")
        full_on = getint("fan_full_on")
        pwm_frequency = getint("pwm_frequency")
        interval = getint("interval")
        minimal_fan_speed = getint("minimal_fan_speed")
        fan_pin = getint("fan_pin")
        fan.ChangeFrequency(pwm_frequency)
        if not(fan_pin_p == fan_pin):
            GPIO.cleanup()
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BOARD)
            GPIO.setup(fan_pin,GPIO.OUT)
            fan = GPIO.PWM(fan_pin,pwm_frequency)
            fan.start(100)
            time.sleep(1)
            fan_pin_p = fan_pin
        max_temp = gettemp()
        max_temp = float(max_temp)
        if debug:
            print(f"highest temp:\t {max_temp}")
        if fan_off:
            off = (getint("fan_off") + getint("buffer"))
        else:
            off = getint("fan_off")
        if max_temp>off:
            if max_temp<full_on:
                speed = mapval(max_temp, off, full_on, 0, 100)
                if speed < minimal_fan_speed:
                    speed = minimal_fan_speed
            else:
                speed = 100
            fan_off = False
        else:
            speed = 0
            fan_off = True
        if rasp:
            fan.ChangeDutyCycle(speed)
            if debug:
                print("speed:\t\t", str(speed) + "%")
            time.sleep(interval)
        else:
            print("speed:\t\t", str(speed) + "%")
        if debug:
            print("\n")            
except KeyboardInterrupt: # tap CTRL+C for keyboard interrupt
    if rasp:
        GPIO.cleanup()
### end of fan.py ###
'''
