import sys, glob, os,time
from subprocess import call
def make_file(file):
    print(f'creating {file}...')
    code=[]
    with open(sys.argv[0], 'r') as f:
        lines = f.readlines()
    main_area = False
    for line in lines:
        if line == (f'### end of {file} ###') or line == f'### end of {file} ###\n':
            main_area = False
            break
        if main_area:
            code.append(line.replace('###',''))
        if line == f'### start of {file} ###\n':
            main_area = True
    with open(file, 'w') as f:
        for line in code:
            f.write(line)
    print('done')
    return(code)
make_file("fan.py")
make_file("fan.sh")
make_file("config.cf")
print('delete install files')
os.chdir("./")
if os.path.isfile("./install_slave.sh"):
    os.unlink("install_slave.sh")
os.unlink("install.py")
print("done")
print('edit config.cf settings')
#input('press enter to continue')
exit()
'''
### start of fan.sh ###
### sudo python3 ./fan.py
### end of fan.sh ###
### start of config.cf ###
master_addr
timeout 10
### end of config.cf ###
### start of fan.py ###
import socket
def getint(name):
    val = ""
    config_file=open('config.cf','r')
    line = config_file.readline()
    while not(name in line):
        line = config_file.readline()
    config_file.close()
    string = str(line.replace(name,"").replace(" ",""))
    this = ""
    for byte in string:
         try:
             int(byte)
             val += byte
         except:
            while False:
                print("this does not execute")
    return int(val)
def getstring(name):
    val = ""
    config_file=open('config.cf','r')
    line = config_file.readline()
    while not(name in line):
        line = config_file.readline()
    config_file.close()
    string = str(line.replace(name,"").replace(" ",""))
    return(string)
def send(addr):
    t=open("/sys/class/thermal/thermal_zone0/temp")
    _temp = t.readline()
    t.close()
    _temp = float(_temp)/1000
    ip = addr.split(":")[0]
    port = int(addr.split(":")[1])
    sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
    sock.sendto(bytes(str(_temp), 'utf-8'), (ip, port))
    sock.close()
    return(addr)
def get_ip_address():
    ip_address = '';
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8",80))
    ip_address = s.getsockname()[0]
    s.close()
    return ip_address
print(get_ip_address())
while True:
    try:
        addr = getstring("master_addr")
        ip = addr.split(":")[0]
        if (ip == get_ip_address()):
            port = int(addr.split(":")[1])-1
        else:
            port = int(addr.split(":")[1])
        ask = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        ask.settimeout(getint("timeout"))
        ask.bind((get_ip_address(),int(port)))
        want = ask.recvfrom(1)
        ask.close()
        print("send")
        if (True):
            print(send(addr))
        else:
            print("not asked")
    except KeyboardInterrupt:
        break
        exit()
    except:
        print("fail")
        print(f'ip: {getstring("master_addr")}')
### end of fan.py ###
'''

