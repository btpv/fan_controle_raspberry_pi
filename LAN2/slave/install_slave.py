import os
import requests


def make_file(file):
    print(f'creating {file}...')
    r = requests.get(
        f"https://gitlab.com/btpv/fan_controle_raspberry_pi/-/raw/master/LAN2/slave/files/{file}")
    with open(file, 'w') as f:
        f.write(r.text)
    print('done')
    return(r.text)


make_file("fan.py")
make_file("fan.sh")
make_file("config.yaml")
print('delete install files')
os.chdir("./")
if os.path.isfile("./install_slave.sh"):
    os.unlink("install_slave.sh")
os.unlink("install.py")
print("done")
print('edit config.cf settings')
#input('press enter to continue')
exit()
