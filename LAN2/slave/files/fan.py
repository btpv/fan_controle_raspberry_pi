from flask import Flask

app = Flask(__name__)

@app.route("/")
def temp():
    t = open("/sys/class/thermal/thermal_zone0/temp")
    _temp = t.readline()
    t.close()
    _temp = float(_temp)/1000
    return f'{{"temp":{str(_temp)}}}'
app.run("0.0.0.0")
