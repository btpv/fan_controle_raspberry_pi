import time,yaml
import signal
def handle_sigterm(* args):
    raise KeyboardInterrupt()
signal.signal(signal.SIGTERM, handle_sigterm)
class config:
    def get(name):
        with open('config.yaml', 'r') as config:
            cfg = yaml.safe_load(config.read())
        return cfg[name]
    def set(name, value):
        with open('config.yaml', 'r') as config:
            cfg = yaml.safe_load(config.read())
        cfg[name] = value
        with open('config.yaml', 'w') as config:
            config.write(yaml.dump(cfg))

rasp = config.get("raspberry")
debug = config.get("debug")
addr = config.get("addr")
off = config.get("fan_off")
full_on = config.get("fan_full_on")
pwm_frequency = config.get("pwm_frequency")
interval = config.get("interval")
minimal_fan_speed = config.get("minimal_fan_speed")
fan_pin = config.get("fan_pin")
fan_pin_p = -1
mqtt = config.get("mqtt")
mqtt_enabled = False
if mqtt == None:
    if (debug):
        print("no MQTT in config")
else:
    mqtt_enabled = mqtt["enabled"]
    if not mqtt_enabled:
        if (debug):
            print("MQTT disabled")
    else:
        if (debug):
            print("starting MQTT")
        import paho.mqtt.client as mqttlib
        client = mqttlib.Client(str(mqtt["clientid"]))
        client.connect(mqtt["broker"])
    
    
if rasp:
    import RPi.GPIO as GPIO
    import os,requests,json
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(fan_pin, GPIO.OUT)
    fan = GPIO.PWM(fan_pin, pwm_frequency)
    fan.start(100)
    time.sleep(1)
    fan_pin_p = fan_pin
    try:
        res = os.popen('hostname -I').readline().split(" ")[0]
        print(f'ip: {res}')
    except:
        print('failed to get ip address')
    if debug:
        print('start')
    else:
        pass
else:
    debug = True
if debug:
    print("LAN addr: " + str(addr) + " fan_off: " +
          str(off) + " fan_full_on " + str(full_on))
speed = 0


def mapval(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


def gettemp():
    try:
        if rasp:
            temp = []
            t = open("/sys/class/thermal/thermal_zone0/temp")
            _temp = t.readline()
            t.close()
            _temp = float(_temp)/1000
            if debug:
                print("temp:\t\t {0}".format(_temp))
            if mqtt_enabled:
                client.publish(str(mqtt["tempraturetopic"]).replace(
                    "{id}", "1"), round(float(_temp), mqtt["decimals"]))
            temp.append(_temp)
            if debug:
                print("get LAN addr`s:\t " + str(addr))
            if (addr.__len__() < 1):
                return float(_temp)
            for i,_addr in enumerate(addr):
                if debug:
                    print("get LAN addr:\t " + str(_addr))
                ip = _addr.split(":")[0]
                port = _addr.split(":")[1]
                try:
                    _temp = json.loads(requests.get("http://"+_addr).text)["temp"]
                    if debug:
                        print(f'{ip}\'s temp: {_temp}')
                    if mqtt_enabled and "{id}" in mqtt["tempraturetopic"]:
                        client.publish(str(mqtt["tempraturetopic"]).replace("{id}",str(i+2)), round(float(_temp),mqtt["decimals"]))
                    temp.append(_temp)
                except Exception as e:
                    if debug:
                        print("fail to get temperature of \"" + str(_addr) + "\"error:\n",e)

            if debug:
                print("temp:\t\t {0}".format(temp))
            temperature = max(temp)
            return float(temperature)
    except Exception as e:
        if debug:
            print("fail to get temp error:\n",e)
        try:
            t.close()
        except:
            try:
                t = open("/sys/class/thermal/thermal_zone0/temp")
                _temp = t.readline()
                t.close()
                _temp = float(_temp)/1000
            except:
                pass
        return full_on
    if not(rasp):
        temp = []
        temp.append(int(input("enter my temp: ")))
        for _addr in addr:
            print(_addr, "temp")
            _temp = int(input("enter " + str(_addr) + " temp: "))
            temp.append(_temp)
        print(temp)
        temperature = max(temp)
        print(temperature)
        return int(temperature)


fan_off = False
try:
    while True:
        if debug:
            print("\n")
        debug = config.get("debug")
        addr = config.get("addr")
        full_on = config.get("fan_full_on")
        pwm_frequency = config.get("pwm_frequency")
        interval = config.get("interval")
        minimal_fan_speed = config.get("minimal_fan_speed")
        fan_pin = config.get("fan_pin")
        if rasp:
            fan.ChangeFrequency(pwm_frequency)
        if not(fan_pin_p == fan_pin):
            if rasp:
                GPIO.cleanup()
                GPIO.setwarnings(False)
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(fan_pin, GPIO.OUT)
                fan = GPIO.PWM(fan_pin, pwm_frequency)
                fan.start(100)
                time.sleep(1)
                fan_pin_p = fan_pin
        max_temp = gettemp()
        max_temp = float(max_temp)
        if mqtt_enabled:
            client.publish(str(mqtt["tempraturetopic"]).replace("{id}", "0"), round(max_temp, mqtt["decimals"]))
        if debug:
            print(f"highest temp:\t {max_temp}")
        if fan_off:
            off = (config.get("fan_off") + config.get("buffer"))
        else:
            off = config.get("fan_off")
        if max_temp > off:
            if max_temp < full_on:
                speed = mapval(max_temp, off, full_on, 0, 100)
                if speed < minimal_fan_speed:
                    speed = minimal_fan_speed
            else:
                speed = 100
            fan_off = False
        else:
            speed = 0
            fan_off = True
        if mqtt_enabled:
            client.publish(mqtt["fanspeedtopic"],
                           round(speed, mqtt["decimals"]))
        if rasp:
            fan.ChangeDutyCycle(speed)
            if debug:
                print("speed:\t\t", str(speed) + "%")
            time.sleep(interval)
        else:
            print("speed:\t\t", str(speed) + "%")
            time.sleep(interval)
        if debug:
            print("\n")
except KeyboardInterrupt:  # tap CTRL+C for keyboard interrupt
    if rasp:
        GPIO.cleanup()
    if mqtt_enabled:
        client.publish(str(mqtt["fanspeedtopic"]), -1)
        for i in range(len(addr)+2):
            time.sleep(0.5)	
            client.publish(str(mqtt["tempraturetopic"]).replace("{id}", str(i)), -1)
	
        
        
