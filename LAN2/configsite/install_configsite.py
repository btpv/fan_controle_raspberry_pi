import os
import requests


def make_file(file):
    print(f'creating {file}...')
    r = requests.get(
        f"https://gitlab.com/btpv/fan_controle_raspberry_pi/-/raw/master/LAN2/configsite/files/{file}")
    with open(file, 'w') as f:
        f.write(r.text)
    print('done')
    return(r.text)


make_file("main.py")
make_file("run.sh")
make_file("index.html")
make_file("remoteaddes.html")
make_file("mqtt.html")
print('delete install files')
os.chdir("./")
if os.path.isfile("./install_config.sh"):
    os.unlink("install_config.sh")
os.unlink("install.py")
print("done")
exit()
