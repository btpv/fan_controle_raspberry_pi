from flask import Flask
from flask import request
import urllib.parse
import yaml

app = Flask(__name__)

redirect = (
    """
        <!DOCTYPE HTML>
        <html lang="en-US">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="refresh" content="0; url=thisurl">
                <script type="text/javascript">
                    window.location.href = "thisurl"
                </script>
                <title>Page Redirection</title>
            </head>
            <body>
                If you are not redirected automatically, follow this <a href='thisurl'>link</a>.
            </body>
        </html>
        """
)


class oldconfig:
    def getstring(name):
        config_file = open('config.cf', 'r')
        line = config_file.readline()
        while not(name in line):
            line = config_file.readline()
        config_file.close()
        return((line.replace(name, "")+" ")[1:-1])

    def getbool(name):
        config_file = open('config.cf', 'r')
        line = config_file.readline()
        while not(name in line):
            line = config_file.readline()
        config_file.close()
        return("True" in line or "true" in line)

    def getlist(name):
        val = []
        config_file = open('config.cf', 'r')
        line = config_file.readline()
        while not(name in line):
            line = config_file.readline()
        if (not(("[" in line) and ("]" in line))):
            config_file.close()
            return([])
        else:
            config_file.close()
            string = str(line.replace(name, "").replace(" ", "").replace(
                "[", "").replace("]", "").replace("\n", ""))
            if (string == ""):
                config_file.close()
                return([])
            this = ""
            for byte in string:
                if byte == ",":
                    try:
                        val.append(int(this))
                    except:
                        val.append(this)
                    this = ""
                else:
                    this += byte
            try:
                val.append(int(this))
            except:
                val.append(this)
        return (val)

    def getint(name):
        val = ""
        config_file = open('config.cf', 'r')
        line = config_file.readline()
        while not(name in line):
            line = config_file.readline()
        config_file.close()
        string = str(line.replace(name, "").replace(" ", ""))
        this = ""
        for byte in string:
            try:
                int(byte)
                val += byte
            except:
                pass
        return int(val)

    def change(name, value):
        name = str(name)
        value = str(value)
        if config.getbool("debug"):
            print(name)
            print(value)
        old = ""
        with open("config.cf", "r") as f:
            lines = f.readlines()
        for line in lines:
            old += line
        if name not in old:
            return
        else:
            new = ''
            if config.getbool("debug"):
                print("\nold:\n"+old)
            for line in old.split("\n"):
                if name in line:
                    new += name+" "+value+"\n"
                else:
                    new += line + "\n"
            if config.getbool("debug"):
                print("\nnew:\n"+new)
        while new.endswith("\n"):
            new = new[0:-1]
        with open("config.cf", "w") as f:
            f.write(new)


class config:
    def get(name):
        with open('config.yaml', 'r') as config:
            cfg = yaml.safe_load(config.read())
        return cfg[name]

    def set(name, value):
        with open('config.yaml', 'r') as config:
            cfg = yaml.safe_load(config.read())
        cfg[name] = value
        with open('config.yaml', 'w') as config:
            config.write(yaml.dump(cfg))



def getrawurldata(url):
    if(len(url.split("?")) == 2):
        rawdata = urllib.parse.unquote(str(url.split("?")[1]))
        return rawdata
    else:
        return []


def geturldata(url):
    if(len(url.split("?")) == 2):
        rawdata = urllib.parse.unquote(str(url.split("?")[1]))
        values = "{\""+rawdata.replace("=", "\":\"").replace("&",
                                                             "\",\"").replace(":,", ":\"\",")+"\"}"
        import ast
        dict = ast.literal_eval(values)
        return dict
    else:
        return {}


@app.after_request
def treat_as_plain_text(response):
    response.headers["content-type"] = "text/html"
    return response


@app.route("/add")
def add():
    if(len(request.url.split("?")) > 1):
        data = geturldata(request.url)
        for index in list(data):
            value = data[index]
            if value == "":
                data.pop(index)
        if "ip" and "port" in data:
            old = config.get("addr")
            if config.get("debug"):
                print(old)
                print(data["ip"]+":"+data["port"])
                print(len(config.get("addr")))
            old.append(data["ip"]+":"+data["port"])
            config.set("addr", old)
            if config.get("debug"):
                print(old)
    return(redirect.replace("thisurl", "http://"+request.host+"/"))


@app.route("/remove")
def remove():
    if(len(request.url.split("?")) > 1):
        data = geturldata(request.url)
        print(data)
        for index in list(data):
            value = data[index]
            print(value)
            old = list(config.get("addr"))
            old.pop(old.index(value))
            config.set("addr", old)
    return(redirect.replace("thisurl", "http://"+request.host+"/"))


@app.route("/")
def main():
    # data = "\""+getrawurldata(request.url).replace("&","\"&\"").replace("=","\"=\"")+"\""
    if(len(request.url.split("?")) > 1):
        data = geturldata(request.url)
        for index in list(data):
            value = data[index]
            if value == "":
                data.pop(index)
        if "raspberry" not in data:
            data.update({"raspberry": False})
        if "debug" not in data:
            data.update({"debug": False})
        for index in list(data):
            value = str(data[index])
            try:
                value = int(value)
            except:
                if value.lower() == "true":
                    value = True
                elif value.lower() == "false":
                    value = False
            config.set(index, value)
        return(
            """
            <!DOCTYPE HTML>
            <html lang="en-US">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="refresh" content="0; url=thisurl">
                    <script type="text/javascript">
                        window.location.href = "thisurl"
                    </script>
                    <title>Page Redirection</title>
                </head>
                <body>
                    If you are not redirected automatically, follow this <a href='thisurl'>link</a>.
                </body>
            </html>
            """.replace("thisurl", request.url.split("?")[0])
        )
    with open("index.html", "r") as f:
        file = f.read()
    result = file
    result = result.replace("__debug__", "checked" if(
        config.get("debug"))else "")
    result = result.replace("__raspberry__", "checked" if(
        config.get("raspberry"))else "")
    result = result.replace("__pwm_frequency__", str(
        config.get("pwm_frequency")))
    result = result.replace("__interval__", str(config.get("interval")))
    result = result.replace("__minimal_fan_speed__", str(
        config.get("minimal_fan_speed")))
    result = result.replace("__fan_off__", str(config.get("fan_off")))
    result = result.replace(
        "__fan_full_on__", str(config.get("fan_full_on")))
    result = result.replace("__buffer__", str(config.get("buffer")))
    result = result.replace("__fan_pin__", str(config.get("fan_pin")))
    result = result.replace("__timeout__", str(config.get("timeout")))
    lines = ""
    with open("remoteaddes.html", "r") as f:
        for line in f:
            lines += line
    slave_list = ""
    for item in config.get("addr"):
        slave_list += lines.replace("__ip__", item.split(":")
                                    [0]).replace("__port__", item.split(":")[1]) + "<br>"
    result = result.replace("__slave_list__", slave_list[0:-4])
    with open("mqtt.html", "r") as f:
        file = f.read()
    result = result.replace("__mqtt__", file.replace(
        "__mqtt_enabled__", "checked" if (config.get("mqtt")["enabled"]) else "").replace(
        "__broker__", config.get("mqtt")["broker"]).replace(
        "__clientid__", config.get("mqtt")["clientid"]).replace(
        "__decimals__", str(config.get("mqtt")["decimals"])).replace(
        "__fanspeedtopic__", config.get("mqtt")["fanspeedtopic"]).replace(
        "__tempraturetopic__", config.get("mqtt")["tempraturetopic"]))
    return(result)


@app.route("/mqtt")
def mqtt():
    data = geturldata(request.url)
    for index in list(data):
        value = data[index]
        if value == "":
            data.pop(index)
    if "enabled" not in data:
        data.update({"enabled": False})
    for index in list(data):
        value = str(data[index])
        try:
            value = int(value)
        except:
            if value.lower() == "true":
                value = True
            elif value.lower() == "false":
                value = False
        data.update({index: value})
    print(config.get("mqtt"))
    print(data)
    config.set("mqtt",data)
    return(redirect.replace("thisurl", "http://"+request.host+"/"))
if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
