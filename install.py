import sys, glob, os,time
from subprocess import call
def make_file(file):
    print(f'creating {file}...')
    code=[]
    with open(sys.argv[0], 'r') as f:
        lines = f.readlines()
    main_area = False
    for line in lines:
        if line == (f'### end of {file} ###') or line == f'### end of {file} ###\n':
            main_area = False
            break
        if main_area:
            code.append(line.replace('###',''))
        if line == f'### start of {file} ###\n':
            main_area = True
    with open(file, 'w') as f:
        for line in code:
            f.write(line)
    print('done')
    return(code)
make_file("fan.py")
make_file("fan.sh")
make_file("config.cf")
print('delete install files')
os.chdir("./")
if os.path.isfile("./install.sh"):
    os.unlink("install.sh")
os.unlink("install.py")
print("done")
print('edit config.cf settings')
#input('press enter to continue')
exit()
'''
### start of fan.sh ###
### sudo python3 .fan.py
### end of fan.sh ###
### start of config.cf ###
debug True
raspberry True
pwm_frequency 100
interval 1
minimal_fan_speed 20
fan_off 30
fan_full_on 50
fan_pin 11
addr []
### end of config.cf ###
### start of fan.py ###
def getbool(name):
    config_file=open('config.cf','r')
    line = config_file.readline()
    while not(name in line):
        line = config_file.readline()
    config_file.close()
    return("True" in line or "true" in line)
def getlist(name):
    val=[]
    config_file=open('config.cf','r')
    line = config_file.readline()
    while not(name in line):
        line = config_file.readline()
    if (not(("[" in line) and ("]" in line))):
        config_file.close()
        return([])
    else:
        config_file.close()
        string = str(line.replace(name,"").replace(" ","").replace("[","").replace("]","").replace("\n",""))
        if (string == ""):
            config_file.close()
            return([])
        this = ""
        for byte in string:
            if byte==",":
                try:
                    val.append(int(this))
                except:
                    val.append(this)
                this = ""
            else:
                this += byte
        try:
            val.append(int(this))
        except:
            val.append(this)
    return (val)
def getint(name):
    val = ""
    config_file=open('config.cf','r')
    line = config_file.readline()
    while not(name in line):
        line = config_file.readline()
    config_file.close()
    string = str(line.replace(name,"").replace(" ",""))
    this = ""
    for byte in string:
         try:
             int(byte)
             val += byte
         except:
            while False:
                print("this does not execute")
    return int(val)
rasp = getbool("raspberry")
debug = getbool("debug")
addr = getlist("addr")
off = getint("fan_off")
full_on = getint("fan_full_on")
pwm_frequency = getint("pwm_frequency")
interval = getint("interval")
minimal_fan_speed = getint("minimal_fan_speed")
fan_pin = getint("fan_pin")
import time
import json
if rasp:
    import RPi.GPIO as GPIO
    import smbus
    import signal
    import sys
    import os
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(fan_pin,GPIO.OUT)
    fan = GPIO.PWM(fan_pin,pwm_frequency)
    fan.start(100)
    time.sleep(1)
    fan_pin_p = fan_pin
    i2c = smbus.SMBus(1)
    if debug:
        print('start')
    else:
        while False:
            print("this does not execute")
else:
    debug = True
##    for _addr in addr:
##        i2c.write_byte(_addr,0xFF)
if debug:
    print ("I2C addr: " + str(addr) + " fan_off: " + str(off) + " fan_full_on " + str(full_on))
speed = 0
def mapval(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
def gettemp():
    try:
        if rasp:
            temp = []
            #res = os.popen('vcgencmd measure_temp').readline()
            #_temp = res.replace("temp=","").replace("'C\n","").replace(".0","")
            t=open("/sys/class/thermal/thermal_zone0/temp")
            _temp = t.readline()
            t.close()
            _temp = float(_temp)/1000
            if debug:
                print("temp:\t\t {0}".format(_temp))
            temp.append(_temp)
            if debug:
                print("get I2C addr`s:\t " + str(addr))
            if (addr.__len__() < 1):
                return float(_temp)
            for _addr in addr:
                if debug:
                    print("get lan addr:\t " + str(_addr))
                _temp = i2c.read_byte(_addr)
                _temp = float(_temp) 
                temp.append(_temp)
            if debug:
                print("temp:\t\t {0}".format(temp))
            temperature = max(temp)
            return float(temperature)
    except:
        if debug:
            print("fail to get temp")
        try:
            t.close()
        except:
            while False:
                print("this does not execute")
        return full_on
    if not(rasp):
        temp = []
        temp.append(int(input("enter my temp: ")))
        for _addr in addr:
            print(_addr, "temp")
            _temp = int(input("enter " + str(_addr) + " temp: "))
            temp.append(_temp)
        print(temp)
        temperature = max(temp)
        print(temperature)    
        return int(temperature)
try:    
    while True:
        if debug:
            print("\n")
        debug = getbool("debug")
        addr = getlist("addr")
        off = getint("fan_off")
        full_on = getint("fan_full_on")
        pwm_frequency = getint("pwm_frequency")
        interval = getint("interval")
        minimal_fan_speed = getint("minimal_fan_speed")
        fan_pin = getint("fan_pin")
        fan.ChangeFrequency(pwm_frequency)
        if not(fan_pin_p == fan_pin):
            GPIO.cleanup()
            GPIO.setwarnings(False)
            GPIO.setmode(GPIO.BOARD)
            GPIO.setup(fan_pin,GPIO.OUT)
            fan = GPIO.PWM(fan_pin,pwm_frequency)
            fan.start(100)
            time.sleep(1)
            fan_pin_p = fan_pin
        max_temp = gettemp()
        max_temp = float(max_temp)
        if debug:
            print(f"highest temp:\t {max_temp}")
        if max_temp>off:
            if max_temp<full_on:
                speed = mapval(max_temp, off, full_on, 0, 100)
                if speed < minimal_fan_speed:
                    speed = minimal_fan_speed
            else:
                speed = 100
        else:
            speed = 0
        if rasp:
            fan.ChangeDutyCycle(speed)
            if debug:
                print("speed:\t\t", str(speed) + "%")
            time.sleep(interval)
        else:
            print("speed:\t\t", str(speed) + "%")
        if debug:
            print("\n")
            
except KeyboardInterrupt: # trap a CTRL+C keyboard interrupt
    if rasp:
        GPIO.cleanup()
### end of fan.py ###
'''
